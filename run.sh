#!/bin/zsh

case "$1" in
  "dev")
  python run.py &
  P1=$!
  cd client && yarn start &
  P2=$!
  wait $P1 $P2
  ;;
  "prod")
  cd client && yarn build &
  P1=$!
  ;;
  *)
   echo "Invalid option provided. Options: dev / prod"
   exit 1
   ;;
esac
