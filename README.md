# Air Logic 
## Coding Test 
### Candidate: Jacob Bickerstaff 

Hello there, 

I have developed this application on my MACOS computer and do not have access to a Windows PC to test a build script. I 
apologise for not providing one. 

You'll need to set up a python virtual environment, first, then activate it accordingly and then install the 
required modules, found in requirements.txt. Once this is complete the server side of the application has been set up.

The client side of the application is built in React.js and is located in the client folder.  run 'yarn' package manager.
Once the packages are installed, return to the root directory and run 'run.bat'.

Thanks, 
Jacob 
jacoblukebickerstaff@gmail.com
07919022202

Node version: v14.16.0 
Python version: 3.7

