const state = {
    artist: {
        name: null,
        albums: [],
        average_song_length: 0,
        total_number_of_tracks: 0
    }
}

export default state;