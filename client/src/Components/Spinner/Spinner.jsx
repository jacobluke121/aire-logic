import ClipLoader from 'react-spinners/ClipLoader';

import './Spinner.scss';

const Spinner = ({ size }) => {
  return (
    <div className={'spinner'}>
      <ClipLoader size={size} />
    </div>
  )
}

export default Spinner;
