import React from "react";
import {Route, Switch} from 'react-router-dom';

import Form from '../Form/Form';
import Result from "../Result/Result";

import './App.scss';

const App = () => {
    return (
        <div className="App">
            <h1>Lyrics Words Application</h1>
            <Switch>
                <Route exact path="/" component={Form}/>
                <Route exact path="/result/:artist_1" component={Result}/>
            </Switch>
        </div>
    );
}

export default App;
