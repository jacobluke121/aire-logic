import axios from "axios";
import React, {useState, useEffect, useCallback} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import Spinner from '../Spinner/Spinner';
import {Table} from "react-bootstrap";
import state from "../../STATES/result_state";

import './result.scss';

const Result = () => {
    const history = useHistory();
    const [loading, setLoading] = useState(true);
    const [responseData, setResponseData] = useState({state});

    const {artist_1} = useParams();

    const fetchData = useCallback(() => {
        const ac = new AbortController();
        axios.get('/artist/' + artist_1).then((response) => {
            if (response.data === 'No Artist found') {
                alert('No Artist found with the name ' + artist_1)
                history.push('/');
                return () => ac.abort();
            }
            setResponseData(prevState => ({...prevState, ...response.data}));
            setLoading(false);
        }, (error) => {
            console.log(error);
            alert('No Artist found with the name ' + artist_1)
            history.push('/');
            return () => ac.abort();
        });
    }, [artist_1, history])

    useEffect(() => {
        fetchData();
    }, [fetchData]);


    return (
        <React.Fragment>
            {loading ? <Spinner size={150}/> :
                <div>
                    <p className="explanation">Hello, this application uses the <a
                        href="https://www.last.fm/api#getting-started">last.fm api</a> to
                        obtain information about the artist you entered, such as the albums and tracks associated with
                        them. The Average number of words are calculated by using the information obtained through
                        <i> last.fm api</i> via the <a
                            href="https://lyricsovh.docs.apiary.io/#reference/0/lyrics-of-a-song/search">lyrics.ovh
                            api</a>. The Information provided below isn't necessarily 100% accurate and is limited by
                        the
                        api's mentioned in this text.
                        <br/><br/>
                        Thank you.
                    </p>
                    <div className="tables">
                        <Table hover className={'table-container'}>
                            <thead>
                            <tr className="header-row">
                                <th>Column</th>
                                <th>Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Artist</td>
                                <td>{responseData.artist.name}</td>
                            </tr>
                            <tr>
                                <td>Average Number of Words in their Songs across all albums</td>
                                <td>{responseData.artist.average_song_length}</td>
                            </tr>
                            <tr>
                                <td>Total Number of Tracks collected through the last.fm api</td>
                                <td>{responseData.artist.total_number_of_tracks}</td>
                            </tr>
                            <tr>
                                <td>Total Number of Albums found through the last.fm api</td>
                                <td>{Object.keys(responseData.artist.albums).length}</td>
                            </tr>
                            </tbody>
                        </Table>
                        <Table hover className={'table-container'}>
                            <thead>
                            <tr className="header-row">
                                <th>Albums</th>
                            </tr>
                            </thead>
                            <tbody>
                            {responseData.artist.albums.map(album =>
                                <tr>
                                    <td>{album}</td>
                                </tr>)}
                            </tbody>
                        </Table>
                    </div>
                    <button type='submit'
                            className='btn btn-lg btn-primary'
                            value='Go Back'
                            onClick={() => history.goBack()}>Go Back
                    </button>
                </div>
            }
        </React.Fragment>)
}

export default Result;
