import React from 'react';
import {useHistory} from 'react-router-dom';

import Row from 'react-bootstrap/Row';
import {useForm} from 'react-hook-form';
import {ErrorMessage} from '@hookform/error-message';
import './form.scss';

const Form = () => {
    const {register, handleSubmit, formState: {errors}} = useForm();
    const history = useHistory();

    const onSubmit = (data) => {
        history.push('/result/' + encodeURIComponent(data['Artist']))
    };

    return (
        <div>
            <p>Please enter an Artist or a band name below, then press submit.  Once this is done, we will then provide information
            about the band or artist, including total number of albums and songs, average number of words in each song and the album
            names.  <br/><br/>The Algorithm running on the server may take a few minutes, so please do bare with us! <br/><br/>Thank you!</p>
            <form onSubmit={handleSubmit(onSubmit)} className='form-container'>
                <Row className={'col-4'}>
                    <input type='text' placeholder='Artist or Band name required ...' {...register('Artist',
                        {required: 'Please provide a value for Artist, thank you.'})} />
                    <ErrorMessage className='err-msg' errors={errors} name='Artist' as='p'/>
                    <input type='submit' className='btn'/>
                </Row>
            </form>
        </div>
    );
}

export default Form;