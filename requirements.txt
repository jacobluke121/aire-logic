flask~=1.1.2
flask_cors~=3.0.10
gunicorn~=20.1.0
requests~=2.25.1
aiohttp~=3.7.4
setuptools~=52.0.0
pytest~=6.2.3