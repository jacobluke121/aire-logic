from server.models import Artist, Album, Track
from .track_lists import track_list
import pytest


class TestLyrics():

    @pytest.fixture()
    def setup_tests(self):
        iron_maiden = Artist('Iron Maiden')
        iron_maiden_album = Album('Iron Maiden')
        iron_maiden.albums.append(iron_maiden_album)
        list_of_tracks = []

        for track in track_list:
            new_track = Track(track, ' '.join(track_list[track]))
            list_of_tracks.append(new_track)

        iron_maiden_album.tracks = list_of_tracks
        return iron_maiden

    def test_average_song_lyrics(self, setup_tests):
        total_number_of_lyrics = 104 + 87 + 96 + 182 + 1 + 107 + 260 + 132
        number_of_tracks = 8
        average_song_lyrics = round(total_number_of_lyrics / number_of_tracks, 2)
        test_returned_value = setup_tests.get_avg_song_length()
        assert average_song_lyrics == test_returned_value

    def test_get_lyric_list_size_if_na(self):
        track = Track('new_track', 'na')
        assert track.get_lyrics_list_size() == 0
