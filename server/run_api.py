from server.api_connections.lastFmApi import LastFmApi
from server.json_builder import build_json

if __name__ == '__main__':
    artist = LastFmApi('Iron Maiden')
    print(artist.artist.get_name())
    info = artist.get_artist_info()
    print(build_json(artist.artist))
