from flask import Blueprint
from server.api_connections.lastFmApi import LastFmApi
from server.json_builder import build_json
from flask_cors import cross_origin

api = Blueprint('api', __name__)

@api.route('/artist/<name>', methods=['GET'])
@cross_origin()
def post_artist(name):
    artist = LastFmApi(name)
    print(artist.artist.get_name())
    data = artist.get_artist_info()
    if data[0] == 'No Artist found':
        return 'No Artist found', 200
    json_data = build_json(artist.artist)
    return json_data, 200
