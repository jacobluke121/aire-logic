class Artist:

    def __init__(self, name, mbid=''):
        self.__name = name
        self.__mbid = mbid
        self.__total_number_of_tracks = None
        self.albums = []

    def set_name(self, name):
        self.__name = name

    def get_name(self):
        return self.__name

    def set_mbid(self, mbid):
        self.__mbid = mbid

    def get_mbid(self):
        return self.__mbid

    def set_total_number_of_tracks(self, total_number_of_tracks):
        self.__total_number_of_tracks = total_number_of_tracks

    def get_total_number_of_tracks(self):
        return self.__total_number_of_tracks

    def get_avg_song_length(self):
        track_total = 0
        word_total = 0
        for album in self.albums:
            for track in album.tracks:
                # if it's == 0 then we have not been able to retrieve lyrics
                if track.get_lyrics_list_size() != 0:
                    track_total += 1
                    word_total += track.get_lyrics_list_size()
        if track_total == 0:
            return 0
        self.set_total_number_of_tracks(track_total)
        return round(float(word_total / track_total), 2)

    # used for debugging purposes
    @property
    def print_artist(self):
        return 'Artist name: ' + self.__name


class Album:

    def __init__(self, name='', mbid=''):
        self.__name = name
        self.__mbid = mbid
        self.tracks = []

    def set_name(self, name):
        self.__name = name

    def get_name(self):
        return self.__name

    def set_mbid(self, mbid):
        self.__mbid = mbid

    def get_mbid(self):
        return self.__mbid

    # used for debugging purposes
    @property
    def print_album(self):
        return 'Album name: ' + self.__name


class Track:

    def __init__(self, name='', lyrics=''):
        self.__name = name
        self.__duration = None
        self.__lyrics = lyrics

    def set_name(self, name):
        self.__name = name

    def get_name(self):
        return self.__name

    def set_duration(self, duration):
        self.__duration = duration

    def get_duration(self):
        return self.__duration

    def set_lyrics(self, lyrics):
        self.__lyrics = lyrics

    def get_lyrics(self):
        return self.__lyrics

    def get_lyrics_list_size(self):
        if self.__lyrics == 'na':
            return 0
        return len(self.__lyrics.split())

    # used for debugging purposes
    @property
    def print_track(self):
        return 'Track name: ' + self.__name
