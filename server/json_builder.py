import json


def build_json(artist):
    return json.dumps({'artist': {'name': artist.get_name(),
                                  'albums': [a.get_name() for a in artist.albums],
                                  'average_song_length': artist.get_avg_song_length(),
                                  'total_number_of_tracks': artist.get_total_number_of_tracks()}})
