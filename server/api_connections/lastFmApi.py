from ..models import Artist, Album, Track
import requests  # required
import asyncio
import aiohttp  # required

#  required for windows machines
try:
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
except:
    print('')

class LastFmApi:

    def __init__(self, artist_name):
        self.artist = Artist(artist_name)
        self.URL = 'http://ws.audioscrobbler.com/2.0/'
        self.API_KEY = "0ac517b578debf16d7da96e9b3ad3123",
        self.HEADERS = {'user-agent': 'orangesub121'}  # provided as requested by last.fm docs to avoid being banned!

    def get_artist_info(self):

        payload = {
            'api_key': self.API_KEY,
            'method': 'artist.search',
            'artist': self.artist.get_name(),
            'format': 'json'
        }

        r = requests.get(self.URL, headers=self.HEADERS, params=payload)
        json_result = r.json()
        try:
            if int(json_result['results']['opensearch:totalResults']) == 0:
                return 'No Artist found', 200
            else:  # artist has been found, we will take the first artist
                self.artist.set_mbid(json_result['results']['artistmatches']['artist'][0]['mbid'])
                self.__get_albums()
                # calls lyrics Ovh api function below
                self.__get_lyrics()
        except KeyError:
            return 'No Artist found', 200
        return 'artist found', 200

    def __get_albums(self):
        payload = {
            'api_key': self.API_KEY,
            'method': 'artist.getTopAlbums',
            'mbid': self.artist.get_mbid(),
            'format': 'json'
        }

        r = requests.get(self.URL, headers=self.HEADERS, params=payload)

        albums = r.json()['topalbums']['album']
        albums_list = []

        for album in albums:
            new_album = Album()
            for album_info in album:
                if album_info == 'mbid':
                    new_album.set_mbid(album[album_info])
                if album_info == 'name':
                    new_album.set_name(album[album_info])
            album_and_tracks = self.__get_tracks(new_album)
            if album_and_tracks != 'skip':  # we don't care for albums with no track info!
                albums_list.append(album_and_tracks)
        self.artist.albums = albums_list

    def __get_tracks(self, album):
        payload = {
            'api_key': self.API_KEY,
            'method': 'album.getInfo',
            'mbid': album.get_mbid(),
            'format': 'json'
        }

        r = requests.get(self.URL, headers=self.HEADERS, params=payload)

        if r.json().get('error'):
            return 'skip'  # error occurred with mbid value 'no album found'
        else:
            track_list = []
            tracks = r.json()['album']['tracks']['track']
            for track in tracks:
                new_track = Track()
                for track_info in track:
                    if track_info == 'duration':
                        new_track.set_duration(track[track_info])
                    if track_info == 'name':
                        new_track.set_name(track[track_info])
                track_list.append(new_track)
            album.tracks = track_list
            return album

    def __get_lyrics(self):
        for album in self.artist.albums:
            track_list = []
            for track in album.tracks:
                track_list.append(track.get_name())
            lyrics_list = asyncio.run(self.__get_lyrics_async(self.artist.get_name(), track_list))
            for lyrics, track in zip(lyrics_list, album.tracks):
                track.set_lyrics(lyrics[1])

    async def __get_lyrics_async(self, artist, tracks_list):
        async with aiohttp.ClientSession() as session:
            tasks = []
            for track in tracks_list:
                task = asyncio.ensure_future(self.__get_lyrics_data(session, track, artist))
                tasks.append(task)
            return await asyncio.gather(*tasks)

    @staticmethod
    async def __get_lyrics_data(session, track, artist):
        url = f'https://api.lyrics.ovh/v1/{artist}/{track}'
        try:
            # timeout set as the api runs for 2 minutes if track on their database, 5 seconds seemed to be optimal.
            async with session.get(url, timeout=5) as response:
                result_data = await response.json()
                return track, result_data['lyrics']
        except:  # broad but acceptable in this case given we don't care what the error is.
            return track, 'na'
