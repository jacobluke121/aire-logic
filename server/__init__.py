from flask import Flask,send_from_directory, Blueprint
from server import view
from flask_cors import CORS

app = Flask(__name__, static_folder='../client/build', static_url_path='')

cors = CORS(app)

main_app = Blueprint('main_app', __name__)


@main_app.route('/')
def serve():
    return send_from_directory(app.static_folder, 'index.html')


app.register_blueprint(main_app)
app.register_blueprint(view.api)
