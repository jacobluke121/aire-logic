from setuptools import setup

setup(
    name='Lyrics Application',
    version='1.0',
    author='Jacob Bickerstaff',
    long_description=__doc__,
    packages=['server', 'server/api_connections'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask~=1.1.2',
        'requests~=2.25.1',
        'aiohttp~=3.7.4',
        'setuptools~=52.0.0'
    ]
)
